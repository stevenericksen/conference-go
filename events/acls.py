import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

def get_photo(city, state):
    # Use the Pexels API
    query = city + ", " + state
    headers = {'Authorization': PEXELS_API_KEY}
    params = {"query": query, "per_page": 1}
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, headers=headers, params=params)
    if response.status_code == 200:
        response = response.json()
        if response.get("photos"):
            return response["photos"][0]["src"]["original"]
        else:
            return None
    else:
        return None

def get_weather_data(city, state):
    params = {
        "q": city + ", " + state,
        "appid": OPEN_WEATHER_API_KEY
    }
    geoloc = requests.get(
        "http://api.openweathermap.org/geo/1.0/direct", params=params
    )

    if geoloc.status_code == 200:
        geoloc = geoloc.json()
        params = {
            "appid": OPEN_WEATHER_API_KEY,
            "lat": geoloc[0]["lat"],
            "lon": geoloc[0]["lon"],
            "units": "imperial",
        }
        weather = requests.get(
            "https://api.openweathermap.org/data/2.5/weather", params=params
        )
        if weather.status_code == 200:
            weather = weather.json()

            description = weather["weather"][0]["description"]
            temp = weather["main"]["temp"]

            return description, temp
        else:
            return None, None
    else:
        return None, None
